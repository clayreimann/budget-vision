export default class Mortgage {
  constructor(principle, apr, term, paymentOpts = {}) {
    this.principle = principle;
    this.apr = apr;
    this.term = term;
    this.paymentOpts = paymentOpts;

    const {periodicRate, standardPayment} = this._calculateScheduledPayment();
    this.periodicRate = periodicRate;
    this.standardPayment = standardPayment;
    this.actualPayment = this.calculatePayment(paymentOpts);
  }

  calculateRepaymentSchedule({usingOptions = true} = {}) {
    const schedule = [];
    const payment = usingOptions ? this.actualPayment : this.standardPayment;

    let remaining = this.principle;
    let period = 0;
    while (period < this.term) {
      if (remaining <= 0) {
        break;
      }
      period += 1;

      const interestPaid = remaining * this.periodicRate;
      const thisPayment = Math.min(remaining + interestPaid, payment);
      const principlePaid = Math.min(remaining, thisPayment - interestPaid);
      remaining = remaining - principlePaid;
      schedule.push({
        period,
        principlePaid,
        interestPaid,
        payment: Math.min(payment, thisPayment),
        principleBalance: remaining,
      });
    }

    return schedule;
  }

  _calculateScheduledPayment() {
    const periodicRate = (this.apr / 12) / 100;
    const simplePayment = this.principle / this.term;
    const standardPayment = this.principle * (
      periodicRate /
      (1 - Math.pow(1 + periodicRate, -(this.term)))
    );

    return {
      periodicRate,
      standardPayment: standardPayment || simplePayment,
    };
  }

  calculatePayment({extraAmt, extraPct}) {
    if (extraAmt) {
      let payment = this.standardPayment + extraAmt;
      console.log(`computed ${payment} with extraAmt: ${extraAmt}`);
      return payment;
    }

    if (extraPct) {
      let payment = this.standardPayment * (1 + extraPct / 100);
      console.log(`computed ${payment} with extraPct: ${extraPct}`);
      return payment;
    }

    console.log(`return inital value ${this.standardPayment}`);
    return this.standardPayment;
  }
}
