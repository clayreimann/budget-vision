import Vue from 'vue';
import Router from 'vue-router';
import HomePage from '@/components/HomePage';
import Calculator from '@/components/Calculator';

Vue.use(Router);

export default new Router({
  routes: [
    { path: '/', name: 'Home', component: HomePage },
    { path: '/calculator', name: 'Calculator', component: Calculator, props: (route) => (route.query) },
  ],
});
