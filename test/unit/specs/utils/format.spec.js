import { formatMoney, formatDollars } from '@/utils/format';

describe('Number formatting', function() {
  const amounts = [
    10.0,
    10.1,
    10.15,
    10.51,
    1000,
    1000.15,
    1000.51,
  ];

  it('formats money correctly', function() {
    const formatted = [
      '$10.00',
      '$10.10',
      '$10.15',
      '$10.51',
      '$1,000.00',
      '$1,000.15',
      '$1,000.51',
    ];
    amounts.forEach((amount, i) => {
      expect(formatMoney(amount)).to.equal(formatted[i]);
    });
  });

  it('formats whole dollar amounts correctly', function() {
    const formatted = [
      '10',
      '10',
      '10',
      '11',
      '1,000',
      '1,000',
      '1,001',
    ];
    amounts.forEach((amount, i) => {
      expect(formatDollars(amount)).to.equal(formatted[i]);
    });
  });

  it('formats fractional dollar amounts correctly', function() {
    const formatted = [
      '10.00',
      '10.10',
      '10.15',
      '10.51',
      '1,000.00',
      '1,000.15',
      '1,000.51',
    ];
    amounts.forEach((amount, i) => {
      expect(formatDollars(amount, false)).to.equal(formatted[i]);
    });
  });
});
