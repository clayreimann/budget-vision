export function getComponent(Constructor, propsData = {}) {
  return new Constructor({ propsData }).$mount();
}
