import numeral from 'numeral';

function clampCents(number) {
  return Math.round(number * 100) / 100;
}

export function formatMoney(number) {
  return numeral(clampCents(number)).format('$0,0.00');
}

export function formatDollars(number, whole = true) {
  return numeral(clampCents(number)).format(`0,0${whole ? '' : '.00'}`);
}
