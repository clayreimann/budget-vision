import Mortgage from '@/utils/mortgage';

describe('Morgage calculator', function() {
  describe('standard mortgages', function() {
    it('correctly calculates loans with no interest', function() {
      const m = new Mortgage(12, 0, 12);
      expect(m.standardPayment, 'standardPayment').to.equal(1);
      expect(m.actualPayment, 'actualPayment').to.equal(1);
      expect(m.periodicRate, 'rate').to.equal(0);
    });

    it('correctly calculates loans with interest', function() {
      const m = new Mortgage(12, 12, 1);
      const rounded = Math.round(m.standardPayment * 100) / 100;
      expect(rounded, 'standardPayment').to.equal(12.12);
      expect(m.periodicRate, 'rate').to.equal(0.01);
    });
  });

  describe('accelerated repayment', function() {
    const extraAmt = 100;
    const extraPct = 50;

    it('correctly adds an extra amount', function() {
      const m = new Mortgage(12, 0, 12, {extraAmt});
      expect(m.actualPayment).to.equal(101);
    });

    it('correctly adds an extra percentage', function() {
      const m = new Mortgage(12, 0, 12, {extraPct});
      expect(m.actualPayment).to.equal(1.50);
    });

    it('only adds an extra amount when both options are specified', function() {
      const m = new Mortgage(12, 0, 12, {extraAmt, extraPct});
      expect(m.actualPayment).to.equal(101);
    });

    it('returns the inital amount when no options are specified', function() {
      const m = new Mortgage(12, 0, 12, {notARealOption: 100});
      expect(m.actualPayment).to.equal(1);
    });
  });

  describe('amortization tables', function() {
    const mortgage = new Mortgage(12, 0, 12, {extraAmt: 1.5});
    it('can disregard extra options', function() {
      const payments = mortgage.calculateRepaymentSchedule({usingOptions: false});
      expect(payments.length).to.equal(12);
      expect(payments).to.eql([
        {period: 1, principlePaid: 1, interestPaid: 0, payment: 1, principleBalance: 11},
        {period: 2, principlePaid: 1, interestPaid: 0, payment: 1, principleBalance: 10},
        {period: 3, principlePaid: 1, interestPaid: 0, payment: 1, principleBalance: 9},
        {period: 4, principlePaid: 1, interestPaid: 0, payment: 1, principleBalance: 8},
        {period: 5, principlePaid: 1, interestPaid: 0, payment: 1, principleBalance: 7},
        {period: 6, principlePaid: 1, interestPaid: 0, payment: 1, principleBalance: 6},
        {period: 7, principlePaid: 1, interestPaid: 0, payment: 1, principleBalance: 5},
        {period: 8, principlePaid: 1, interestPaid: 0, payment: 1, principleBalance: 4},
        {period: 9, principlePaid: 1, interestPaid: 0, payment: 1, principleBalance: 3},
        {period: 10, principlePaid: 1, interestPaid: 0, payment: 1, principleBalance: 2},
        {period: 11, principlePaid: 1, interestPaid: 0, payment: 1, principleBalance: 1},
        {period: 12, principlePaid: 1, interestPaid: 0, payment: 1, principleBalance: 0},
      ]);
    });

    it('uses extra options', function() {
      const payments = mortgage.calculateRepaymentSchedule();
      expect(payments.length).to.equal(5);
      expect(payments).to.eql([
        {period: 1, principlePaid: 2.5, interestPaid: 0, payment: 2.5, principleBalance: 9.5},
        {period: 2, principlePaid: 2.5, interestPaid: 0, payment: 2.5, principleBalance: 7},
        {period: 3, principlePaid: 2.5, interestPaid: 0, payment: 2.5, principleBalance: 4.5},
        {period: 4, principlePaid: 2.5, interestPaid: 0, payment: 2.5, principleBalance: 2},
        {period: 5, principlePaid: 2, interestPaid: 0, payment: 2, principleBalance: 0},
      ]);
    });
  });
});
