import Vue from 'vue';
import Calculator from '@/components/Calculator';
import { getComponent } from './helpers';

const Constructor = Vue.extend(Calculator);

describe.skip('Calculator.vue', function() {
  describe('render', function() {
    it('renders a title', function() {
      const calc = getComponent(Constructor);
      const h1 = calc.$el.querySelector('.calculator h1');
      expect(h1.textContent).to.equal('Loan Calculator');
    });

    it('does not render results automatically', function() {
      const calc = getComponent(Constructor);
      const results = calc.$el.querySelector('#results');
      expect(results).to.equal(null);
    });
  });

  describe('query props', function() {
    it('gets default data without query props', function() {
      const data = Calculator.data();
      expect(data.principle, 'principle').to.equal(100000);
      expect(data.apr, 'apr').to.equal(6);
      expect(data.term, 'term').to.equal(360);
      expect(data.extraPct, 'extraPct').to.equal(0);
      expect(data.extraAmt, 'extraAmt').to.equal(0);
      expect(data.payments, 'payments').to.eql([]);
      expect(data.monthlyPayment, 'monthlyPayment').to.equal(0);
    });

    it('correctly reads query props', function() {
      const a = 100;
      const p = 50;
      const t = 12;
      const amt = 20;
      const pct = 10;

      const calc = getComponent(Constructor, {a, p, t, amt, pct});
      expect(calc.principle, 'principle').to.equal(p);
      expect(calc.apr, 'apr').to.equal(a);
      expect(calc.term, 'term').to.equal(t);
      expect(calc.extraPct, 'extraPct').to.equal(pct);
      expect(calc.extraAmt, 'extraAmt').to.equal(amt);
      expect(calc.payments, 'payments').to.eql([]);
      expect(calc.monthlyPayment, 'monthlyPayment').to.equal(0);
    });
  });

  describe('computed properties', function() {
    const payments = [
      {payment: 2, interestPaid: 1},
      {payment: 2, interestPaid: 1},
      {payment: 2, interestPaid: 1},
      {payment: 2, interestPaid: 1},
    ];
    it('correctly sums interest', function(done) {
      const calc = getComponent(Constructor);
      expect(calc.interestPaid).to.equal(0);

      calc.payments = payments;
      Vue.nextTick(() => {
        expect(calc.interestPaid).to.equal(4);
        done();
      });
    });

    it('correctly sums total repayment', function(done) {
      const calc = getComponent(Constructor);
      calc.payments = payments;
      Vue.nextTick(() => {
        expect(calc.totalRepayment).to.equal(8);
        done();
      });
    });
  });

  describe('watched properties', function() {
    it('correctly parses principle', function(done) {
      const calc = getComponent(Constructor);
      expect(calc.principle, 'initial').to.equal(100000);
      calc.principle = '$200,000';
      Vue.nextTick(() => {
        expect(calc.principle, 'parsed').to.equal(200000);
        done();
      });
    });

    it('correctly parses extraAmt', function(done) {
      const calc = getComponent(Constructor);
      expect(calc.extraAmt, 'initial').to.equal(0);
      calc.extraAmt = '$50';
      Vue.nextTick(() => {
        expect(calc.extraAmt, 'parsed').to.equal(50);
        done();
      });
    });
  });
});
